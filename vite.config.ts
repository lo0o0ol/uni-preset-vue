import { defineConfig } from "vite";
import { resolve } from 'path';

import uni from "@dcloudio/vite-plugin-uni";


const pathResolve = (dir: string): any => {
  return resolve(__dirname, '.', dir);

};


const alias: Record<string, string> = {
  '@/': pathResolve('./src/')
};

// https://vitejs.dev/config/

export default defineConfig({
  plugins: [uni()],
  resolve: { alias }
});
