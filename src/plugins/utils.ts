// import Vue from 'vue';
// #ifdef APP-PLUS
// import { judgePermission } from './permission';
// #endif

interface SystemInfo {
    statusBarHeight: number;
    navBarH: number;
    titleBarHeight: number;
    platform?: string;
  }


interface LocationData {
  tabbarH: number;
  tabbarPaddingB: number;
  device: 'iOS' | 'Android';
}

//获取系统信息、判断ipX安全距离
// export const getTabbarHeight = function (): LocationData {
//   var systemInfo = uni.getSystemInfoSync();
//   var data = {
//     ...systemInfo,
//     tabbarH: 50, // tabbar高度--单位px
//     tabbarPaddingB: 0, // tabbar底部安全距离高度--单位px
//     device: systemInfo.system.indexOf('iOS') != -1 ? 'iOS' : 'Android', //苹果或者安卓设备
//   }
//   let modelArr = ['10,3', '10,6', 'X', 'XR', 'XS', '11', '12', '13', '14', '15', '16'];
//   let model = systemInfo.model;
//   model && modelArr.forEach(item => {
//     //适配iphoneX以上的底部，给tabbar一定高度的padding-bottom
//     if (model.indexOf(item) != -1 && (model.indexOf('iPhone') != -1 || model.indexOf('iphone') != -1)) {
//       data.tabbarH = 70
//       data.tabbarPaddingB = 20
//     }
//   })
//   return data;
// }

