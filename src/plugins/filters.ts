
export default {
    install(Vue: any) {
        // value 需要过滤的数据，例如["数学", "语文", "英语"]
        // space 分隔符
        Vue.filter('strArrFormat', (value: any, space: any) => {
            let str: any = '';
            if (value && value.length > 0) {
                value.forEach((ele: any) => {
                    if (str) {
                        str += space;
                    }
                    str += ele;
                });
            }
            return str;
        });
        //金额过滤
        Vue.filter('money', function (val: any) {
            if (val) {
                let value = Math.round(parseFloat(val) * 100) / 100;
                let valMoney = value.toString().split(".");
                if (valMoney.length == 1) {
                    value = value.toString() + ".00" as any;
                    return value;
                }
                if (valMoney.length > 1) {
                    if (valMoney[1].length < 2) {
                        value = value.toString() + "0" as any;
                    }
                    return value;
                }
                return value;
            } else {
                return "0.00";
            }
        });

        //手机号中间4位为*
        Vue.filter('phone', function (val: string) {
            var tel = val;
            tel = "" + tel;
            var telShort = tel.replace(tel.substring(3, 7), "****")
            return telShort;
        });
    },
};