
declare module '@/uni_modules/uview-plus';

declare module 'uview-plus' {
    global {
        interface Uni {
            $u: any
        }
    }
}

declare namespace UniApp {
    interface GetPhoneNumberResult {
      encryptedData: string;
      iv: string;
      errMsg: string;
    }
  
    interface Uni {
      getPhoneNumber(options: {
        success?(res: GetPhoneNumberResult): void;
        fail?(err: any): void;
        complete?(): void;
      }): void;
    }
  }