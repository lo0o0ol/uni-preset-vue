import "@/style/index.scss";

import { createSSRApp } from "vue";
import App from "./App.vue";
import * as Pinia from "pinia";
import uviewPlus from "@/uni_modules/uview-plus";

import base from "@/config/baseUrl";

import "@/utils/request";

import Mock from "./mock"



import CommonConponent from "@/components/common/index.vue";
import Navbar from '@/components/Navbar/index.vue';
import Iconfont from "@/components/Iconfont/index.vue";
export function createApp() {
  const app = createSSRApp(App);


  app.use(Pinia.createPinia());
  app.use(uviewPlus);
  app.config.globalProperties.$baseUrl = base.baseUrl;


  app.component("CommonConponent", CommonConponent);
  app.component("Navbar", Navbar);
  app.component("Iconfont", Iconfont);
  return {
    app,
    Pinia
  };
}
