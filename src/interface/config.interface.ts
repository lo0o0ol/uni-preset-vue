interface capsuleItem {
    height: number;
    top: number;
    width: number;
}

export interface SystemInfo {
    statusBarHeight?: number;
    navBarH: number;
    titleBarHeight: number;
    screenWidth: number;
    platform?: string;
    capsule: capsuleItem;
}

export interface ShareConfig {
    title: string;
    desc: string;
    link: string;
    imgUrl: string;
}

export interface CourtConfig {
    publicAppId: string;
    baseUrl: string;
    systemInfo: SystemInfo;
    mapData: {
        key: string;
        sk: string;
    };
    share: {
        title: string;
        desc: string;
        link: string;
        imgUrl: string;
    };
}
export interface envItem {
    name: string;
    value: string;
    icon?: string;
}
