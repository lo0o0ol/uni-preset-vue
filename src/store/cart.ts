import { defineStore } from "pinia";

// 示例 TS 类型定义

interface cartItem {
    id: number;
    name: string;
    thumbnailUrl: string;
    description: string;
    fixedMoney: string;
}
interface CartState {
    cartList: cartItem[];
    totalPrice:string
}

export const useCartStore = defineStore("cart", {
    // state 定义全局的一些变量，通过actions里添加方法进行修改
    state: (): CartState => ({
        cartList: [
            {
                id: 1,
                name: "烤椰自由卡",
                description: "可享5杯烤椰系列免单",
                thumbnailUrl:
                    "https://oss.xiaokacoffee.com/2023-12-04/c5003a9bb3ef4497bf41c5f4b872dedd.jpg",
                fixedMoney: "45.9",
            },
            {
                id: 2,
                name: "暖冬必喝双杯套餐",
                description: "生椰拿铁+燕麦鸳鸯拿铁",
                thumbnailUrl:
                    "https://oss.xiaokacoffee.com/2023-11-23/1d218e45e7024daca685cfe79ab913fb.jpg",
                fixedMoney: "26.5",
            },
            {
                id: 3,
                name: "干杯芝士抹茶",
                description: "一口顺滑（不含咖啡）",
                thumbnailUrl:
                    "https://oss.xiaokacoffee.com/2023-12-22/92c856b915d04591b2c5349cee3cbfe9.jpg",
                fixedMoney: "18",
            },
            {
                id: 4,
                name: "山楂茉莉",
                description: "",
                thumbnailUrl:
                    "https://oss.xiaokacoffee.com/2023-07-26/29e019909933484db51aede297b9324d.png",
                fixedMoney: "8.8",
            },
            {
                id: 5,
                name: "超能粉椰美式",
                description: "香气馥郁 口感柔和",
                thumbnailUrl:
                    "https://oss.xiaokacoffee.com/2023-07-31/b476cccf017e494bb3811a37d8e54d44.png",
                fixedMoney: "19",
            },
        ],
        totalPrice:"118.2"
    }),

    actions: {
        setCartList(cartList: cartItem[]): void {
            this.cartList = cartList;
            this.totalPrice = "0.00"
        },
    },
});
export function useAppStoreHook() {
    return useCartStore();
}
