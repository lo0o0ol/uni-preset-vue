import { defineStore } from 'pinia';

// 示例 TS 类型定义

interface UserInfo {
  userId: number;
  name: string;
  phone: string;
  useNum?:number;
  token?: string
}
interface AppState {
  envUrl: string;
  userInfo: UserInfo;
  coopen: boolean;
  pageLoading: boolean;
  loginPopupShow: boolean;
  showWxLogin: boolean;
  PrimaryColor: string;
  currentRouter: string;
  pickTack: number
}

export const useAppStore = defineStore("app", {
  // state 定义全局的一些变量，通过actions里添加方法进行修改
  state: (): AppState => ({
    envUrl: "",
    // { name: '本地', value: "http://localhost:8080/api" },
    // { name: '测试', value: "https://go.heytea.com/api" },
    // { name: '与线上', value: "http://localhost:8080/api" },

    userInfo: {
      userId: 1,
      phone: '19525492954',
      name: '隔壁老王',
    },
    coopen: true,// 首页开屏 默认开启
    pageLoading: false, // 加载动画
    loginPopupShow: false, // 是否打开登录授权弹窗
    showWxLogin: false, // 微信授权弹窗
    PrimaryColor: '#E73E20',// 主题色
    currentRouter: '',// 当前路径
    pickTack: 10,
  }),

  actions: {
    setCoopen(coopen: boolean): void {
      this.coopen = coopen;
    },
    setUserInfo(data: Partial<UserInfo>): void {
      this.userInfo = { ...this.userInfo, ...data };
      console.log(this.userInfo)

    },
    setPageLoading(pageLoading: boolean): void {
      this.pageLoading = pageLoading;
    },
    setLoginPopupShow(loginPopupShow: boolean): void {
      this.loginPopupShow = loginPopupShow
    },
    setShowWxLogin(showWxLogin: boolean): void {
      this.showWxLogin = showWxLogin
    },
    setCurrentRouter(currentRouter: string): void {
      this.currentRouter = currentRouter
    },
    setEnvUrl(envUrl: string): void {
      this.envUrl = envUrl
    },
    setPickTack(pickTack: number): void {
      this.pickTack = pickTack
    },
  },

});
export function useAppStoreHook() {
  return useAppStore();
}
