// 定义 showConfirmDialog 函数，接收参数 title、content、successCallback（成功时调用）、failCallback（失败时调用）
export function showConfirmModal(title: any, content:string, confirmText:string,cancelText:string,successCallback: () => any, failCallback: () => any) {
    // 调用 uni.showModal 方法显示模态对话框
    uni.showModal({
      title: title,   // 设置标题
      content: content,   // 设置内容
      confirmText:confirmText?confirmText: '确定',   // 设置确定按钮的文字
      cancelText:cancelText?cancelText: '取消',   // 设置取消按钮的文字
      success: function (res) {   // 点击确定按钮后触发的回调函数
        if (res.confirm) {
          // 如果用户点击了确定按钮，则执行 successCallback 函数
          successCallback && successCallback();
        } else if (res.cancel) {
          // 如果用户点击了取消按钮，则执行 failCallback 函数
          failCallback && failCallback();
        }
      },
      fail: function () {   // 调用失败时触发的回调函数
        console.log('调用 uni.showModal 方法失败');
      }
    });
  }
