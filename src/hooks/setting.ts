import { computed } from 'vue'
import { useAppStore } from "@/store/app";

export function useAppSetting() {
    const appStore = useAppStore();
    const coopen = computed(() => appStore.coopen);
    const pageLoading = computed(() => appStore.pageLoading);
    const userInfo = computed(() => appStore.userInfo);
    const loginPopupShow = computed(() => appStore.loginPopupShow);
    const currentRoute = computed(() => appStore.currentRouter);
    const PrimaryColor = computed(() => appStore.PrimaryColor);
    const envUrl = computed(() => appStore.envUrl);
    const pickTack = computed(() => appStore.pickTack);
    return {
        coopen,
        pageLoading,
        loginPopupShow,
        currentRoute,
        userInfo,
        PrimaryColor,
        envUrl,
        pickTack
    }
}
