import { computed } from "vue";
import { useCartStore } from "@/store/cart";

export function useCart() {
    const appStore = useCartStore();
    const cartList = computed(() => appStore.cartList);
    const totalPrice = computed(() => appStore.totalPrice);
    return {
        cartList,
        totalPrice,
    };
}
