export interface BannerItem {
    id: string;
    isNewRecord: boolean;
    remarks: string;
    createDate: string;
    updateDate: string;
    name: string;
    url: string;
    beginDate: string;
    endDate: string;
    sort: number;
    status: string;
    linkUrl: string;
    subordinate: string;
    xcxType: string;
    storeId: string;
    bannerJumpType: string;
    typeName: string;
    linkName: string;
  }
  