

export default {
    props: {
        list: {
            type: Array,
            default: []
        },
        current:{
            type: Number,
            default: 0
        },
        width: {
            type: String,
            default: '750'
        },
        height: {
            type: String,
            default: '640'
        },
        autoplay: {
            type: Boolean,
            default: true
        },
        circular: {
            type: Boolean,
            default: true
        },
        keyName:{
            type: String,
            default: 'url'
        },
        typeName: {
            type: String,
            default: ''
        },
        videoValue: {
            type: String,
            default: 'video'
        },
        imgValue: {
            type: String,
            default: 'image'
        },
        autoPlayVideo: {
            type: Boolean,
            default: false
        },
        linkName: {
            type: String,
            default: ''
        },
        interval: {
            type: Number,
            default: 3000
        },
        indicatorPos:{
            type: String,
            default: 'center'
        }

    }
}
