const colorTheme = "#303133"; //字体、左边箭头颜色等默认颜色
export default {
  props: {
    // 导航栏文字
    title: {
      type: String,
      default: "",
    },
    // navbarType等于5透明背景时title是否显示
    isShowTransparentTitle: {
      type: Boolean,
      default: function () {
        return true;
      },
    },
    // 左边文字
    leftText: {
      type: String,
    },
    // 背景颜色
    bgColor: {
      type: String,
      default: "#fff",
    },
    // 背景图片
    image: {
      type: String,
    },
    // 背景图片mode
    imageMode: {
      type: String,
      default: "aspectFill",
    },
    // 导航状态 0、默认固定在顶部 1、不固定在顶部 2、自定义点击事件 3、同时显示箭头和去首页按钮 4、不显示左侧内容 5、上拉渐变显示背景色
    navbarType: {
      type: [String, Number],
      default: 0,
    },
    // 是否显示左侧内容
    isShowLeft: {
      type: Boolean,
      default: function () {
        return true;
      },
    },
    // 左边按钮icon
    leftIcon: {
      type: String,
      default: "arrow-left",
    },
    // 左边icon颜色
    leftIconColor: {
      type: String,
      default: '#303133',
    },
    // 屏幕滑动距离顶部距离(透明固定导航比传)
    scrollTop: {
      type: Number,
      default: 0,
    },
    //导航字体颜色，字体颜色为白色的时候会把手机状态栏设置为白色，否则为黑色
    fontColor: {
      type: String,
      default: colorTheme,
    },
    // navbarType等于5透明背景时title颜色
    transparentTitleColor: {
      type: String,
    },
    titleWidth: {
      type: [String, Number],
      default: 400,
    },
    fontSize: {
      type: [String, Number],
      default: 30,
    },
    // 背景渐变色
    gradient: {
      type: String,
    },
    // 是否设置防止塌陷高度
    isFillHeight: {
      type: Boolean,
      default: function () {
        return true;
      },
    },
  },
};
