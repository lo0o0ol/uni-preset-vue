export default {
    props:{
        title:{
            type:String,
            default:'标题'
        },
        image:{
            type:String,
            default:""
        },
        show:{
            type:Boolean,
            default:false
        }
        
    }
}