// 微信/支付宝小程序---手机号授权登录时使用
function getPhoneInfo(info: any, successCallback: Function, errCallback: Function) {
    let httpData: any = {};
    
    // #ifdef MP-WEIXIN
    httpData = {
        code: info.code,
        iv: info.iv,
        encryptedData: info.encryptedData,
    };
    // #endif
    
    // #ifdef MP-ALIPAY
    httpData = {
        code: '',
        iv: '',
        encryptedData: info,
    };
    // #endif
    
    // 此时需要您的接口返回个人信息
    // uni.$u.http.post('您的接口', httpData).then(res => {
    // var loginInfo = {
    //     userId: res.id,
    //     sessionId: res.sessionId,
    //     isRegister: res.isRegister,
    //     userName: res.userName,
    //     userType: res.userType,
    //     openId: res.openId,
    // };
    successCallback && successCallback();
    // }, err => {
    //     errCallback && errCallback(err)
    // });
}

// 微信/支付宝小程序---通用授权个人信息登录
function getUserInfo(successCallback: Function, errorCallback: Function) {
    uni.showLoading({
        title: '正在申请授权',
    });
    
    // #ifdef MP-WEIXIN
    uni.getUserProfile({
        desc: '用于完善会员资料',
        success: function (res) {
            uni.hideLoading();
            var offUserInfo = res.userInfo;
            successCallback && successCallback(offUserInfo);
        },
        fail: (res) => {
            uni.hideLoading();
            errorCallback && errorCallback(res);
        },
    });
    // #endif
    
    // #ifdef MP-ALIPAY
    uni.getOpenUserInfo({
        success: (res) => {
            uni.hideLoading();
            var offUserInfo = JSON.parse(res.response).response; // 以下方的报文格式解析两层 response
            offUserInfo.avatarUrl = offUserInfo.avatar;
            successCallback && successCallback(offUserInfo);
        },
        fail: (res) => {
            uni.hideLoading();
            console.log(res, "失败");
            errorCallback && errorCallback(res);
        },
    });
    // #endif
}

export {
    getPhoneInfo, // 小程序手机号授权
    getUserInfo, // 小程序个人信息授权
};
