import Request from "@/uni_modules/uview-plus/libs/luch-request/index.js";
import base from "@/config/baseUrl";
import { useAppStore } from "@/store/app";

let envUrl = uni.getStorageSync("envUrl");

let throttleLogin = true;

// Initialize request configuration
uni.$u.http.setConfig((config: any) => {
    config.baseURL = envUrl ? envUrl : base.baseUrl; /* Root domain */
    config.header = {
        "Content-Type": "application/x-www-form-urlencoded",
    };
    config.custom = {
        load: true, // Show loading animation
        isFactory: true, // true: only return data for successful responses, false: return entire response
        catch: true, // Default to catch for unsuccessful responses
    };
    return config;
});

// Request interceptor
uni.$u.http.interceptors.request.use(
    async (config: any) => {
        config.data = config.data || {};
        if (config?.custom?.auth) {
            // config.header.token = store.state.userInfo.token;
        }

        if (config?.custom?.load) {
            // Open loading animation
            useAppStore().setPageLoading(true);
            // store.commit("setLoadingShow", true);
        }

        return config;
    },
    (config: any) => {
        return Promise.reject(config);
    }
);

/**
 * Avoid infinite loop, create a new instance for re-requesting
 */
const againHttp = new Request({
    baseURL: envUrl ? envUrl : base.baseUrl,
});

againHttp.interceptors.request.use(
    (config: any) => {
        // config.header.Authorization = store.state.userInfo.token || (uni.getStorageSync('userInfo').token || '');
        return config;
    },
    (error: any) => {
        return Promise.reject(error);
    }
);

// Response interceptor
uni.$u.http.interceptors.response.use(
    async (response: any) => {
        // Close loading animation
        // store.commit("setLoadingShow", false);
        useAppStore().setPageLoading(false);
        const data = response.data;
        const custom = response.config?.custom;

        if (data.code == 0 || data.code == -1) {
            if (data.code == -1) {
                data.data = [];
            }
            if (!custom.isFactory) {
                return data;
            } else {
                return data.data === undefined ? {} : data.data;
            }
        } else if (data.code == 2) {
            // Token invalid/forced logout
            // Token refresh without user awareness (Uncomment the block below if needed)
            /*
        try {
            const { code } = await getToken();
            if (code == 1) {
                try {
                    const localResponse = await againHttp.middleware(response.config);
                    if (!custom.isFactory) {
                        return localResponse.data;
                    } else {
                        return localResponse.data.data === undefined ? {} : localResponse.data.data;
                    }
                } catch (e) {
                    // Clear login information
                    store.commit("emptyUserInfo");
                    // Re-login
                    judgeLogin();
                    return Promise.reject(e);
                }
            } else {
                // Clear login information
                store.commit("emptyUserInfo");
                // Re-login
                judgeLogin();
                return Promise.reject(data);
            }
        } catch (e) {
            // Handle potential network issues; you can handle this based on your logic
            return Promise.reject(data);
        }
        */

            // Direct login on token expiration (Remove the token refresh block above if using this method)
            // store.commit("emptyUserInfo"); // Clear login information
            // Throttle login for 15 seconds
            if (throttleLogin) {
                throttleLogin = false;
                setTimeout(() => {
                    throttleLogin = true;
                }, 15000);
                // judgeLogin();
            }
            return new Promise(() => {});
        } else {
            // If custom.toast parameter is not explicitly set to false, toast will be shown for errors
            if (custom.toast !== false) {
                uni.$u.toast(data.message || data.msg);
            }
            if (!custom.isFactory) {
                return data;
            } else {
                // If catch is needed, reject; otherwise, return a pending promise so that the request doesn't enter the catch block
                if (custom?.catch) {
                    return Promise.reject(data);
                } else {
                    return new Promise(() => {});
                }
            }
        }
    },
    (response: any) => {
        // Close loading animation
        // store.commit("setLoadingShow", false);
        useAppStore().setPageLoading(false);

        // Handle response errors (statusCode !== 200)
        let errorData = "Please check the network or server";
        let message = response.message || response.msg;

        if (message == "request:fail url not in domain list") {
            errorData =
                "Check if the request domain is added to the domain whitelist";
        } else if (message == "request:fail timeout") {
            errorData = "Request timed out: Please check the network";
        } else {
            errorData = message || "Please check the network or server";
        }

        uni.$u.toast(errorData);
        return Promise.reject(response);
    }
);
