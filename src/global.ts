import type { App } from "vue";
import ActivityPopup from '@/components/ZPopup/activity-popup.vue'

export function registerGlobalComponents(app:App): void {
  // Register your global components here
  app.component("ActivityPopup", ActivityPopup);
}
