import Mock from 'better-mock/dist/mock.mp'


import  userInfo from "./user-info.json"

import homeLogin from "./home-login.json"
import diningTypeBanner from "./diningType-banner.json"
import homeMock from './home-data.json';
import productInfoMock from './product-info.json';
import comboMealMock from './combo-meal.json';
import noticeShopMock from './notice-shop.json';
import menuMock from './menu-data.json';

import SingleDetail from "./single-detail.json";
import SingleSpec from "./single-spec.json";

Mock.mock('http://localhost:8080/api/service-portal/vip/grayapi/home-login', 'get', homeLogin);

Mock.mock('http://localhost:8080/api/service-portal/vip/grayapi/user-info', 'get', userInfo);

Mock.mock('http://localhost:8080/api/service-portal/vip/grayapi/diningType-banner', 'get', diningTypeBanner);
Mock.mock('http://localhost:8080/api/service-portal/vip/grayapi/home-data', 'get', homeMock);
Mock.mock('http://localhost:8080/api/service-menu/vip/grayapi/v4/shop/product-info?shopId=3142&productIds=5683&menuType=0&isTakeaway=0', 'get', productInfoMock);
Mock.mock('http://localhost:8080/api/service-menu/vip/grayapi/shop/combo-meal?shopId=601&productIds=5698', 'get', comboMealMock);
Mock.mock('http://localhost:8080/api/service-portal/vip/grayapi/shop/notice/601', 'get', noticeShopMock);
Mock.mock('http://localhost:8080/api/storeMerchandis/getTabByMer/48', 'get', menuMock);

Mock.mock('http://localhost:8080/api/storeMerchandis/getTabByMer/single-detail', 'get', SingleDetail);
Mock.mock('http://localhost:8080/api/storeMerchandis/getTabByMer/single-spec', 'get', SingleSpec);

export default Mock;
