/// <reference types="vite/client" />

declare module '*.vue' {
  import { DefineComponent } from 'vue'
  // eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/ban-types
  const component: DefineComponent<{}, {}, any>
  export default component
}


declare module '@/uni_modules/uview-plus';

declare namespace UniApp {
  interface GetPhoneNumberResult {
    encryptedData: string;
    iv: string;
    errMsg: string;
  }

  interface Uni {
    getPhoneNumber(options: {
      success?(res: GetPhoneNumberResult): void;
      fail?(err: any): void;
      complete?(): void;
    }): void;
  }
}

declare const ROUTES: []
