// import { getTabbarHeight } from '@/plugins/utils';
import type { SystemInfo, CourtConfig } from "@/interface/config.interface";

let baseUrl = "";
if (process.env.NODE_ENV === "development") {
    // Development environment
    baseUrl = "https://crm.xiaokacoffee.com";
    // baseUrl = 'https://go.heytea.com/api';
} else if (process.env.NODE_ENV === "production") {
    // Production environment
    baseUrl = "https://crm.xiaokacoffee.com";
}

let systemInfo: SystemInfo = {
    //   ...getTabbarHeight(),
    // #ifdef MP-ALIPAY
    navBarH:
        (uni.getSystemInfoSync()?.statusBarHeight ?? 0) +
        (uni.getSystemInfoSync()?.titleBarHeight ?? 0),
    titleBarHeight: uni.getSystemInfoSync()?.titleBarHeight ?? 0,
    // #endif
    // #ifdef MP-WEIXIN
    statusBarHeight: uni.getSystemInfoSync().statusBarHeight,
    navBarH: (uni.getSystemInfoSync()?.statusBarHeight ?? 0) + 44,
    titleBarHeight: 44,
    screenWidth: uni.getSystemInfoSync().screenWidth,
    capsule: uni.getMenuButtonBoundingClientRect(),
    // #endif
};

// 平台
// #ifdef MP-WEIXIN
systemInfo.platform = "weixin";
// #endif
// #ifdef MP-ALIPAY
systemInfo.platform = "alipay";
// #endif
// #ifdef MP-TOUTIAO
systemInfo.platform = "toutiao";
// #endif
// #ifdef APP-PLUS
systemInfo.platform = "plus";
// #endif

const courtConfig: CourtConfig = {
    publicAppId: "", // 公众号appId
    baseUrl: baseUrl, // 域名
    systemInfo: systemInfo, // 系统信息
    mapData: {
        key: "", // 地图key
        sk: "",
    },
    share: {
        title: "基于uview2.0快速开发框架", // 分享标题
        desc: "基于uview2.0快速开发框架详情", // 分享详情
        link: "", // 分享链接
        imgUrl: "", // 分享图
    },
};

export default Object.assign({}, courtConfig);
