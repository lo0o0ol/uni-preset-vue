import { computed } from "vue"
// const { userInfo } = useAppSetting()
import { useAppStore } from "@/store/app";
const userInfo = computed(()=>useAppStore().userInfo)

// 微信/支付宝小程序---手机号授权登录时使用
// info: uni.login获取的参数
export const getPhoneInfo = (info: { code: any; iv: any; encryptedData: any; }, successCallback: (arg0: {}) => any, errCallback: any) => {
    let httpData = {}
    // #ifdef MP-WEIXIN
    httpData = {
    	code: info.code, //小程序code
    	iv: info.iv, //小程序加密算法的初始向量
    	encryptedData: info.encryptedData, //包括敏感数据在内的完整用户信息的加密数据
    };
    // #endif
    // #ifdef MP-ALIPAY
	httpData = {
		code: '', //小程序code
		iv: '', //小程序加密算法的初始向量
		encryptedData: info, //包括敏感数据在内的完整用户信息的加密数据
	};
    // #endif

    // 此时需要您的接口返回个人信息
	// uni.$u.http.post('您的接口', httpData).then(res => {
        var loginInfo = {
            // userId: res.id,
            // sessionId: res.sessionId,
            // phoneNum: res.phoneNum,
            // userName: res.userName,
            // openId: res.openId,
        };
        successCallback && successCallback(loginInfo)
    // }, err => {
    //     errCallback && errCallback(err)
    // });
}
// 微信/支付宝小程序---通用授权个人信息登录
export const getUserInfo = (successCallback: (arg0: UniApp.UserInfo) => any,errorCallback: (arg0: any) => any) => {
    uni.showLoading({
        title: '正在申请授权',
    });
    // #ifdef MP-WEIXIN
    uni.getUserProfile({
        desc: '用于完善会员资料',
        success: function(res) {
            uni.hideLoading()
            var offUserInfo = res.userInfo
            successCallback && successCallback(offUserInfo)
        },fail: (res) => {
            uni.hideLoading()
            errorCallback && errorCallback(res)
        }
    })
    // #endif
    // #ifdef MP-ALIPAY
    uni.getOpenUserInfo({
        success: (res: { response: string; }) => {
            uni.hideLoading()
            var offUserInfo = JSON.parse(res.response).response // 以下方的报文格式解析两层 response
            offUserInfo.avatarUrl = offUserInfo.avatar
            successCallback && successCallback(offUserInfo)
        },fail: (res: any) => {
            uni.hideLoading()
            console.log(res, "失败")
            errorCallback && errorCallback(res)
        }
    })
    // #endif
}

let lock = false
let promiseResult:any = []
// 获取token
export const getToken = () => {
	return new Promise((resolve, reject) => {
		promiseResult.push({
			resolve,
			reject
		})
		if (!lock) {
			lock = true
			// uni.login({
			//     success(res){
					var httpData = {
						username: 'ceshi',
						password: '111111',
					}
					uni.$u.http.post('/api/Tokensys/login',httpData,{custom: {isFactory:false}}).then((res: { code: number; token: any; }) => {
						if(res.code==1){
							let userInfo = {
								token:res.token,//token用于判断是否登录
							}
							console.log(res.token,'res.token')
                            useAppStore().setUserInfo(userInfo)

							// uni.$emit("loginCallback") //全局登录监听回调方法
						}
						while (promiseResult.length) {
							// p1.resolve(res.data)
							promiseResult.shift().resolve(res)
						}
						lock = false
					}).catch((err: any) => {
						while (promiseResult.length) {
							// p1.reject(err)
							promiseResult.shift().reject(err)
						}
						lock = false
					})
			//     }
			// })
		}
	})
}

// 获取当前路径
export const getCurrentRouter = (callback?: any) => {
	let routes = getCurrentPages() // 获取当前打开过的页面路由数组
	let curRoute = routes[routes.length - 1].route
	return curRoute
}

var throttleLogin = true
//判断是否登录（所有端）
export const judgeLogin = (callback: () => any) => {
    let storeUserInfo = userInfo
    if (!storeUserInfo.userId){ // nvue页面读取不到vuex里面数据，将取缓存
		// #ifdef APP-APP-NVUE
		 storeUserInfo = uni.getStorageSync('userInfo')
		// #endif
    }
	console.log(storeUserInfo,'storeUserInfo----')
    if (!storeUserInfo.token) {
		if(throttleLogin){
		    throttleLogin = false
			setTimeout(()=>{
			    throttleLogin = true //节流
			},1000)
            let currentRouter = getCurrentRouter() as string
            useAppStore().setCurrentRouter(currentRouter)//获取当前路径
			// #ifdef MP
            useAppStore().setLoginPopupShow(true)
			// #endif
			// #ifdef APP-PLUS
			uni.showModal({
			    title: "登录提示",
			    confirmText:'去登录',
			    cancelText:'再逛会',
			    content:'此时此刻需要您登录喔~',
			})
            uni.navigateTo({
                url: "/pages/user/login"
            });
			// #endif
			// #ifdef H5
			// h5Login();
			// #endif
		}else{
			// uni.$u.toast('您点击的太频繁了')
		}
    }else{
        callback && callback()
    }
}
