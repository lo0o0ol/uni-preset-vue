const http = uni.$u.http




export const getUserInfo = (param={},config={}) => http.get('/service-portal/vip/grayapi/user-info',param,config)

// 获取首页数据
export const getHomeData = (param={},config={}) => http.get('/service-portal/vip/grayapi/home-data',param,config)
export const getHomeLogin = (param={},config={}) => http.get('/service-portal/vip/grayapi/home-login',param,config)

export const getDiningTypeBanner = (param={},config={}) => http.get('/service-portal/vip/grayapi/diningType-banner',param,config)
