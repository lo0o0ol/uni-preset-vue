
const http = uni.$u.http

// 获取套餐商品
export const getComboMeal = (param={},config={}) => http.get('/service-menu/vip/grayapi/shop/combo-meal?shopId=601&productIds=5698',param,config)
export const getProductInfo = (param={},config={}) => http.get('/service-menu/vip/grayapi/v4/shop/product-info?shopId=3142&productIds=5683&menuType=0&isTakeaway=0',param,config)



export const getProductDetail = (param={},config={}) => http.get(`/storeMerchandis/getMerchandiseDetails/${param?.merId}`,param,config)
export const getProductSpec = (param={},config={}) => http.get('/storeMerchandis/getTabByMer/single-spec',param,config)
